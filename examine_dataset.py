from obfy.data import (load_verification_task_from_pan_format,
                       verification_dataset_text_iterator)

import numpy as np
from matplotlib import pyplot as plt


dataset = load_verification_task_from_pan_format('./pan/trial')

lengths = [len(text) for text in verification_dataset_text_iterator(dataset)]

xs = np.linspace(0, 1, num=50)
ys = np.quantile(lengths, xs)
plt.yscale('log')
plt.suptitle('Quantile distribution of post lengths')
plt.plot(xs, ys)
plt.show()
