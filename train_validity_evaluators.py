"""
This script trains a language model that learns to predict the next character
in order to compute its perplexity. This is done to estimate how _realistic_ a
piece of text is.
"""
import pickle

from torch.utils.data import DataLoader
import torch
from torch import nn, optim
from tqdm import tqdm

from obfy.features import char_ngram_tokenizer, tweet_tokenizer
from obfy.dl import get_device, compute_perplexity, Preprocessor, RawTextDataset, Perplexity, TextPredictor, train_model
from obfy.data import (load_verification_task_from_pan_format,
                       verification_dataset_text_iterator)

def _pkl(obj, filename):
    with open(filename, 'wb') as file_:
        pickle.dump(obj, file_)

def get_text_loaders(preprocessor, dataset_train, dataset_val, batch_size,
                device):
    """Helper function to return dataset loaders mounted with a specific
    preprocessor."""
    dataset = RawTextDataset(verification_dataset_text_iterator(dataset_train),
                             preprocessor, device=device)
    dataset_val = RawTextDataset(verification_dataset_text_iterator(dataset_val),
                             preprocessor, device=device)
    train_loader = DataLoader(dataset, batch_size=batch_size)
    val_loader = DataLoader(dataset_val, batch_size=batch_size)
    return train_loader, val_loader

def train_and_save_model(preprocessor, out_file, 
                         embedding_dim,
                         lstm_dim, batch_size=20,
                         epochs=20):
    train_loader, val_loader = get_text_loaders(preprocessor, dataset,
                                                dataset_val, batch_size,
                                           device)

    vocab_size = len(preprocessor.vocab)

    model = TextPredictor(vocab_size, embedding_dim, lstm_dim).to(device)
    criterion = nn.NLLLoss()
    optimizer = optim.RMSprop(model.parameters(), lr=1e-5)

    train_model(model, criterion, optimizer, train_loader, val_loader,
                savefile="{}_model.pkl".format(out_file))
    _pkl(preprocessor, "{}_preprocessor.pkl".format(out_file))

device = get_device()

dataset = load_verification_task_from_pan_format('./pan/verifier_train')[:1000]
dataset_val_full = load_verification_task_from_pan_format('./pan/val')
dataset_val = dataset_val_full[:100]

preprocessor = Preprocessor(tweet_tokenizer(), text_length=220,
                            vocab_params={'min_freq':2})
preprocessor.fit(verification_dataset_text_iterator(dataset))
train_and_save_model(preprocessor, 'out/lstm_tweet', embedding_dim=40,
                     lstm_dim=40, batch_size=20)

preprocessor = Preprocessor(char_ngram_tokenizer(1), text_length=1000,
                            vocab_params={'max_size':1000, 'min_freq':5})
preprocessor.fit(verification_dataset_text_iterator(dataset))
train_and_save_model(preprocessor, 'out/lstm_char', embedding_dim=30,
                     lstm_dim=40, batch_size=40)
