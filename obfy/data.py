"""
Module with helper functions for loading the data.
"""
from random import Random
from pathlib import Path
import os
from collections import namedtuple
from string import ascii_lowercase

import pandas as pd
from bs4 import BeautifulSoup
from tqdm import tqdm
import pandas as pd
import regex

DATA_DIR = "./data/blogs"

def post_filter(post):
    # Function to filter out some posts which break authorship verification
    # software, for example posts which contain no letters or which are very
    # short.
    if len(post) > 4:
        for letter in ascii_lowercase:
            if letter in post:
                return True
    return False

def parse_file(path):
    with open(path, encoding='utf-8', errors='ignore') as file_:
        soup = BeautifulSoup(file_, features='lxml-xml')
    posts = [tag.text.strip() for tag in soup.find_all('post')]
    return [post for post in posts if post_filter(post)]

def get_dataset(dir_path):
    """
    Returns a list of lists of posts of authors from the blogs dataset.
    """
    data = []
    number_of_files = len(os.listdir(DATA_DIR))
    for file_path in tqdm(Path(dir_path).iterdir(), total=number_of_files, desc='Loading data'):
        author_posts = parse_file(file_path)
        if len(author_posts) > 1:
            data.append(author_posts)
    return data

def split_dataset(list_, split_sizes=[0.7,0.2,0.1], random_seed=42):
    if sum(split_sizes) > 1:
        raise ValueError("Total split sizes must sum up to 1 or less")
    random = Random(random_seed)
    indices = random.sample(range(len(list_)), len(list_))
    splits = []
    split_so_far = 0
    for split_size in split_sizes:
        needed_samples = int(len(list_) * split_size)
        next_split_indices = indices[split_so_far:split_so_far+needed_samples]
        splits.append([list_[idx] for idx in next_split_indices])
        split_so_far += needed_samples
    return splits

VerificationItem = namedtuple('VerificationItem', ['posts', 'target', 'label'])

def setup_verification_task(dataset, random_seed=42,
                            fraction_with_false_authors=0.5):
    """
    Turns a list of lists of posts of authors into a verification task, so that
    for each author there is a post given which is either their own or someone
    else's.

    Returns a list of named tuples for each author with attributes
        'posts': <list of posts>
        'target': <string which is a post to verify authorship of>
        'label': <boolean; true if the target is indeed written by the author>

    Basically, for one half of the authors it just takes one of their posts to
    be the target and sets `label` to true. For the other half of the authors,
    it randomly takes one of their posts and sets is as some other authors'
    target.
    """
    random = Random(random_seed)
    num_with_false_authors = int(len(dataset) * fraction_with_false_authors)
    ids_with_false_authors = random.sample(range(0, len(dataset)),
                                           num_with_false_authors)
    # Now we make a mapping from true authors to some other authors
    ids_from = random.sample(ids_with_false_authors,
                             len(ids_with_false_authors))
    ids_to = random.sample(ids_with_false_authors,
                           len(ids_with_false_authors))
    # Need to keep track of which posts we choose to take from one author and
    # hand to another
    post_ids_to_take = {
        id_: random.choice(range(len(dataset[id_]))) 
            for id_ in ids_with_false_authors}
    out_dataset = []
    for id_from, id_to in zip(ids_from, ids_to):
        posts = [post 
                 for idx, post in enumerate(dataset[id_to]) 
                    if idx != post_ids_to_take[id_to]]
        target = dataset[id_from][post_ids_to_take[id_from]]
        # Sometimes, the mapping from ids to ids will have a stationary point
        # and in those cases we can just say that the author is indeed the true
        # one
        if id_from != id_to:
            label = False
        else:
            label = True
        out_dataset.append(
            VerificationItem(posts, target, label))
    ids_with_true_authors = [id_ for id_ in range(0, len(dataset)) if id_ not in
                                   set(ids_with_false_authors)]
    for id_ in ids_with_true_authors:
        chosen_target = random.choice(range(len(dataset[id_])))
        posts = [post 
                 for idx, post in enumerate(dataset[id_]) 
                    if idx != chosen_target]
        target = dataset[id_][chosen_target]
        label = True
        out_dataset.append(
            VerificationItem(posts, target, label))
    random.shuffle(out_dataset)
    return out_dataset

def save_verification_task_in_pan_format(verification_dataset, save_path):
    """
    Saves the dataset in a PAN Authorship Verification task compatible manner.
    This is useful for verification by models used in those tasks.

    A single post is taken from each author and chosen as the 'unknown' to
    be verified by the model.

    :param random_seed: Random seed for picking posts to be the 'unknown'
    """
    truth_map = {}
    for author_id, author in enumerate(verification_dataset):
        author_dir = "EN{:05}".format(author_id)
        path = Path(os.path.join(save_path, author_dir))
        try:
            path.mkdir(parents=True)
        except FileExistsError:
            print("There already is a save in this directory")
        for post_id, post in enumerate(author.posts):
            filename = "known{:03}.txt".format(post_id)
            with open(os.path.join(path, filename), 'w', encoding='utf-8') as file_:
                file_.write(post)
        filename = "unknown.txt"
        with open(os.path.join(path, filename), 'w', encoding='utf-8') as file_:
            file_.write(author.target)
        truth_map[author_dir] = author.label
    with open(os.path.join(save_path, "truth.txt"), 'w', encoding='utf-8') as file_:
        for author, label in sorted(truth_map.items()):
            file_.write("{} {}\n".format(author, 'Y' if label else 'N'))

def _verification_item_dir(path):
    return path.is_dir() and regex.match(r'EN[0-9]+$',path.name)

def _verification_load_labels(filepath):
    """Returns a dictionary of <id>:<label> pairs from a truth.txt file"""
    df = pd.read_csv(filepath, delimiter=' ', names=['id', 'label'])
    df['label'] = (df['label'] == 'Y')
    df_dict = df.to_dict()
    label_map = {
        df_dict['id'][id_]: df_dict['label'][id_]
        for id_ in df_dict['id']}
    return label_map

def load_verification_task_from_pan_format(save_path):
    item_paths = sorted([path for path in Path(save_path).iterdir() if
             _verification_item_dir(path)])
    labels = _verification_load_labels(os.path.join(save_path, 'truth.txt'))
    dataset = []
    for path in item_paths:
        name = path.name
        known_files = [filepath for filepath in path.iterdir() if
                        (filepath.suffix == '.txt' and
                         filepath.name.startswith('known'))]
        unknown_file = os.path.join(path, 'unknown.txt')
        posts = []
        for filepath in known_files:
            with open(filepath) as file_:
                posts.append(file_.read())
        with open(unknown_file) as file_:
            target = file_.read()
        dataset.append(VerificationItem(posts, target, labels[name]))
    return dataset

def verification_dataset_text_iterator(dataset):
    """A generator that yields all text from a verification dataset, including
    targets. This is mainly used for computing global term frequencies."""
    for item in dataset:
        for post in item.posts:
            yield post
        yield item.target

def load_pred_file(path):
    return pd.read_csv(path, delimiter=' ',
                       names=['id', 'label']).sort_values('id',
                                                          ignore_index=True)

def load_truth_file(path):
    df = load_pred_file(path)
    df['label'] = (df['label'] == 'Y')
    return df

