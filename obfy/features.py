"""
Module with helper functions for feature engineering.
"""

from scipy.stats import entropy
from scipy.spatial.distance import cosine
from numpy.linalg import norm
import numpy as np
from nltk.tokenize import TweetTokenizer
from nltk import word_tokenize
from nltk import data as nltk_data

def char_ngrams(text, n):
    ngrams = []
    for i in range(len(text) - n + 1):
        ngrams.append(text[i:i + n])
    return ngrams

class char_ngram_tokenizer:
    def __init__(self, n):
        self.n = n

    def __call__(self, text):
        return char_ngrams(text, self.n)

class tweet_tokenizer:
    """Callable wrapper for TweetTokenizer."""
    def __init__(self):
        self.tokenizer = TweetTokenizer()

    def __call__(self, text):
        return self.tokenizer.tokenize(text)

def cosine_similarity(p, q):
    return cosine(p, q)

def kullback_leibler_divergence(p, q):
    return entropy(p ,q)

def jensen_shannon_divergence(p, q):
    # https://en.wikipedia.org/wiki/Jensen%E2%80%93Shannon_divergence
    p_norm = p / sum(p)
    q_norm = q / sum(q)
    M = 0.5 * (p_norm + q_norm)
    # scipy entropy is KL-divergence
    return 0.5 * (entropy(p_norm, M) + entropy(q_norm, M))

def hellinger_distance(p, q):
    # From https://en.wikipedia.org/wiki/Hellinger_distance
    return norm(np.sqrt(p) - np.sqrt(q), ord=2) / np.sqrt(2)

def skew_divergence(p, q, alpha=0.5):
    # Table 1 from https://www.cs.cornell.edu/home/llee/papers/aistats01.pdf
    return entropy(p, alpha * q + (1 - alpha) * p)

sentence_tokenizer = nltk_data.load('tokenizers/punkt/english.pickle')

def avg_log_sentence_length(text):
    sentences = sentence_tokenizer.tokenize(text)
    if len(sentences) > 0:
        log_lengths = []
        for sentence in sentences:
            log_lengths.append(np.log(len(word_tokenize(sentence))))
        return np.mean(log_lengths)
    else:
        return 0.0
