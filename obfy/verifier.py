"""
Module with content relatede to authorship verifiers.
"""
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.tree import DecisionTreeClassifier
import numpy as np
from tqdm import tqdm
import pandas as pd

from .data import (verification_dataset_text_iterator,
                   load_verification_task_from_pan_format)
from .features import (char_ngram_tokenizer,
                       cosine_similarity,
                       kullback_leibler_divergence,
                       jensen_shannon_divergence,
                       hellinger_distance,
                       skew_divergence,
                       avg_log_sentence_length)

def _validity_test(arr):
    return np.all(np.isfinite(arr)) and not np.any(np.isnan(arr))

class BAFFVerifier:
    """
    Baffling Authorship Verifier from Bevendorff (2019).

    This classifier uses:
        Cosine similarity (TF-weighted)
        Cosine similarity (TFIDF-weighted)
        Kullback-Leibler divergence (KLD)
        Skew divergence (skew-balanced KLD)
        Jensen-Shannon divergence
        Hellinger distance
        Avg. logarithmic sentence length difference

    Refs:
        Janek Bevendorff, Matthias Hagen, Benno Stein, and Martin Potthast.
        2019. Bias Analysis and Mitigation in the Evaluation of Authorship
        Verification. In Proceedings of the 57th Annual Meeting of the
        Association for Computational Linguistics, pages 6301–6306, Florence,
        Italy. Association for Computational Linguistics.
    """
    def __init__(self, classifier=None, tokenizer=None):
        self.cv = None
        self.tf = None
        self.tfidf = None
        if tokenizer:
            self.tokenizer = tokenizer
        else:
            self.tokenizer = char_ngram_tokenizer(3)
        if classifier:
            self.classifier = classifier
        else:
            self.classifier = DecisionTreeClassifier()
        self.normalization_sub = np.zeros([7])
        self.normalization_mul = np.ones([1])

    def train(self, dataset, verbose=False):
        self.cv = CountVectorizer()
        self.tf = TfidfVectorizer(use_idf=False, tokenizer=self.tokenizer)
        self.tfidf = TfidfVectorizer(use_idf=True, tokenizer=self.tokenizer)

        self.cv.fit(verification_dataset_text_iterator(dataset))
        self.tf.fit(verification_dataset_text_iterator(dataset))
        self.tfidf.fit(verification_dataset_text_iterator(dataset))
        train_inputs = []
        train_labels = []
        if verbose:
            for item in tqdm(dataset, desc='Training'):
                sim_vec = self.compute_similarity_vector(
                    " ".join(item.posts),
                    item.target,
                    normalized=False)
                train_inputs.append(sim_vec)
                train_labels.append(item.label)
        else:
            for item in dataset:
                sim_vec = self.compute_similarity_vector(
                    " ".join(item.posts),
                    item.target,
                    normalized=False)
                train_inputs.append(sim_vec)
                train_labels.append(item.label)
        train_inputs = np.array(train_inputs)
        train_labels = np.array(train_labels)
        self._set_normalization(train_inputs)
        train_inputs = self.normalize(train_inputs)
        self.classifier.fit(train_inputs, train_labels)

    def _set_normalization(self, matrix):
        mins = matrix.min(axis=0)
        maxs = matrix.max(axis=0)
        mul = np.array(1)/(maxs-mins)
        self.normalization_sub = mins
        self.normalization_mul = mul

    def normalize(self, matrix):
        return (matrix - self.normalization_sub) * self.normalization_mul

    def compute_similarity_vector(self, text_p, text_q, 
                                  kl_smoothing=1e-7, normalized=True):
        cv_p, cv_q = self.cv.transform([text_p, text_q]).toarray()
        tf_p, tf_q = self.tf.transform([text_p, text_q]).toarray()
        tfidf_p, tfidf_q = self.tfidf.transform([text_p, text_q]).toarray()
        avg_sen_len_diff = np.abs(avg_log_sentence_length(text_p)
                                  -avg_log_sentence_length(text_q))
        sim_vec = np.array([
            cosine_similarity(tf_p, tf_q),
            cosine_similarity(tfidf_p, tfidf_q),
            kullback_leibler_divergence(cv_p + kl_smoothing, 
                                        cv_q + kl_smoothing),
            skew_divergence(cv_p, cv_q),
            jensen_shannon_divergence(cv_p, cv_q),
            hellinger_distance(cv_p, cv_q),
            avg_sen_len_diff])
        if normalized:
            return self.normalize(sim_vec)
        else:
            return sim_vec

    def predict_dataset(self, dataset, verbose=False):
        sim_vecs = []
        if verbose:
            for item in tqdm(dataset, desc='Predicting'):
                sim_vec = self.compute_similarity_vector(
                        " ".join(item.posts),
                        item.target)
                if _validity_test(sim_vec):
                    sim_vecs.append(sim_vec)
                else:
                    sim_vecs.append(np.zeros(7))
        else:
            for item in dataset:
                sim_vec = self.compute_similarity_vector(
                        " ".join(item.posts),
                        item.target)
                if _validity_test(sim_vec):
                    sim_vecs.append(sim_vec)
                else:
                    sim_vecs.append(np.zeros(7))
        sim_vecs = np.array(sim_vecs)
        preds = self.classifier.predict_proba(sim_vecs)
        return preds[:, 1]
    
    def predict(self, item):
        sim_vec = self.compute_similarity_vector(
                    " ".join(item.posts),
                    item.target)
        pred = self.classifier.predict_proba(sim_vec.reshape(1, -1))
        return pred[:, 1]

def evaluate_on_dataset(verifier, dataset_dir, output_file, verbose=False):
    """Runs a verifier on a directory and produces an output text file. 

    The output file is in the format of the PAN authorship verification shared
    task.
    """
    dataset = load_verification_task_from_pan_format(dataset_dir)
    predictions = verifier.predict_dataset(dataset, verbose=True)
    rows = [("EN{:05}".format(id_), prediction) 
            for id_, prediction in enumerate(predictions)]
    df = pd.DataFrame.from_records(rows)
    df.to_csv(output_file, sep=' ', header=False, index=False)
    return df
