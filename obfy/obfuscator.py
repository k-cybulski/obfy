"""
Module with functions for obfuscating authorship.
"""
from random import choice

import nltk
from nltk.corpus import wordnet as wn
from gensim import downloader as gensim_api

from .data import verification_dataset_text_iterator, VerificationItem

def lemmatize_text(text):
    lemmatizer = nltk.WordNetLemmatizer()
    lemmas = [lemmatizer.lemmatize(word) for word in
              nltk.word_tokenize(text)]
    return lemmas

def replace_synonyms(text, synonym_map):
    text_split = text.split(" ") # very easy to roll back
    lemmatizer = nltk.WordNetLemmatizer()
    new_text = []
    for idx, lemma in enumerate([lemmatizer.lemmatize(word) for word in
                                 text_split]):
        if lemma in synonym_map:
            new_text.append(synonym_map[lemma])
        else:
            new_text.append(text_split[idx])
    return " ".join(new_text)

class SynonymObfuscator:
    """A class which finds most common words unique to an author and replaces
    them."""
    def __init__(self, synonym_func, 
                 most_common_n=500,
                 most_common_n_target=200):
        self.fd = nltk.FreqDist()
        self.synonym_func = synonym_func
        self.most_common_n = most_common_n
        self.most_common_n_target = most_common_n_target
        self.most_common_words = None

    def train(self, text_iterator):
        for text in text_iterator:
            self.fd.update(lemmatize_text(text))
        self.most_common_words = set([w[0] for w in
                                      self.fd.most_common(self.most_common_n)])

    def obfuscate(self, target, extra_text=None):
        total_text = target
        if extra_text:
            total_text += extra_text
        author_fd = nltk.FreqDist()
        author_fd.update(lemmatize_text(total_text))
        most_common_target = set([w[0] for w in
                                  author_fd.most_common(self.most_common_n_target)])
        distinct = most_common_target.difference(
            self.most_common_words)
        synonym_map = {}
        for word in distinct:
            synonym = self.synonym_func(word)
            if synonym:
                synonym_map[word] = synonym
        return replace_synonyms(target, synonym_map)

def wnet_synonym(lemma):
    """A very crude method of finding nearby WordNet words."""
    synset = wn.synsets(lemma)
    if not synset:
        return None
    word = synset[0]
    synonyms = set()
    for hypernym in word.hypernyms():
        synonyms = synonyms.union(hypernym.hyponyms())
    for hyponym in word.hyponyms():
        synonyms = synonyms.union(hyponym.hypernyms())
    if word in synonyms:
        synonyms.remove(word)
    if len(synonyms) == 0:
        return None
    synonym = choice(list(synonyms)).lemma_names()[0]
    return synonym.replace('_', ' ') # sometimes, the synonyms are phrases with

def gensim_synonym(lemma, model):
    """A crude synonym finder based on embedding model proximity."""
    try:
        return model.most_similar(lemma)[0][0]
    except KeyError:
        return None

class gensim_synonymizer:
    def __init__(self, model=None):
        if model is None:
            model = gensim_api.load('glove-twitter-25')
        self.model = model

    def synonym(self, word):
        return gensim_synonym(word, self.model)

    def __call__(self, word):
        return self.synonym(word)

def obfuscate_dataset(obfuscator, dataset):
    new_dataset = []
    for item in dataset:
        if item.label:
            new_target = obfuscator.obfuscate(item.target)
            new_dataset.append(
                VerificationItem(item.posts, new_target, item.label)
            )
        else:
            new_dataset.append(item)
    return new_dataset

class RandomObfuscator:
    """Baseline obfuscator which doesn't even attempt to preserve meaning. It
    simply replaces words with random things it has seen before."""
    def __init__(self):
        self.vocab = set()

    def train(self, text_iterator):
        for text in text_iterator:
            self.vocab = self.vocab.union(text.split(" "))

    def obfuscate(self, target, extra_text=None):
        vocab_list = list(self.vocab)
        return " ".join([
            choice(vocab_list) for _ in range(len(target.split(" ")))])

