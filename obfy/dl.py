"""
Module with deep learning models.
"""
from collections import Counter
import pickle

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from torchtext.vocab import Vocab
from ignite.engine import Engine, Events
from ignite.metrics import Metric, RunningAverage
from tqdm import tqdm

from .features import char_ngram_tokenizer
from .data import (load_verification_task_from_pan_format,
                       verification_dataset_text_iterator)

class Preprocessor:
    def __init__(self, tokenize, text_length=None, vocab_params={},
                 pad_at_beginning=False):
        self.tokenize = tokenize
        self.vocab_params = vocab_params
        self.vocab = None
        # If left unset, the text length will be inferred upon fitting as the
        # 80% quantile of lengths
        self.length_set = bool(text_length)
        self.text_length = text_length
        self.pad_at_beginning = pad_at_beginning

    def fit(self, text_iterator):
        counter = Counter()
        lengths = []
        for text in text_iterator:
            tokenized = self.tokenize(text)
            counter.update(tokenized)
            lengths.append(len(tokenized))
        if not self.length_set:
            self.text_length = np.quantile(lengths, 0.9).round().astype(int)
        self.vocab = Vocab(counter, **self.vocab_params)

    def preprocess(self, text):
        tokenized = self.tokenize(text)
        vocabularized = [self.vocab[c] for c in tokenized]
        if len(vocabularized) < self.text_length:
            pad_length = self.text_length - len(vocabularized)
            if self.pad_at_beginning:
                vocabularized = [1] * pad_length + vocabularized
            else:
                vocabularized = vocabularized + [1] * pad_length
        elif len(vocabularized) > self.text_length:
            vocabularized = vocabularized[:self.text_length]
        return vocabularized

    def vocab_size(self):
        return len(self.vocab)

    def __call__(self, text):
        return self.preprocess(text)

class RawTextDataset(Dataset):
    """A simple dataset of text sequences."""
    def __init__(self, text_iterator, preprocessor, device=None):
        """
        Args:
            :text_iterator: Iterator of strings.
            :preprocessor:  Preprocessor which turns strings into sequences of
                            integers.
        """
        self.texts = [preprocessor(text) for text in text_iterator]
        self.device = device

    def __len__(self):
        return len(self.texts)

    def __getitem__(self, idx):
        if self.device:
            return torch.tensor(self.texts[idx]).to(self.device)
        else:
            return torch.tensor(self.texts[idx])

class TextPredictor(nn.Module):
    """A simple LSTM next-token predictor."""
    def __init__(self, vocab_size, embedding_dim, lstm_dim):
        super(TextPredictor, self).__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, lstm_dim, lstm_dim)
        self.linear = nn.Linear(lstm_dim, vocab_size)
        self.logsoftmax = nn.LogSoftmax(dim=2)

    def forward(self, sequence_batch):
        embedded = self.embedding(sequence_batch)
        lstm_out, _ = self.lstm(embedded)
        activated = self.logsoftmax(self.linear(lstm_out))
        output = torch.transpose(activated, 1, 2)
        return output

class Perplexity(Metric):
    """Perplexity of the model on groundtruth data."""
    def __init__(self, output_transform=lambda x: x, device=None):
        self.device = device
        self.perplexities = None
        self.output_transform = output_transform
        super(Perplexity, self).__init__(device=device)

    def reset(self):
        self.perplexities = None
        super(Perplexity, self).reset()

    def update(self, output):
        y_pred, y = self.output_transform(output)

        y_pred_t = torch.transpose(y_pred, 1, 2)

        counts = torch.sum(y != 1, dim=1) # don't take padding into account

        log_probs = torch.take(y_pred_t, y)
        masked_log_probs = torch.where(y != 1, log_probs,
                                       torch.zeros_like(log_probs,
                                                        device=self.device))

        # compute perplexity for each sample
        perplexities = torch.exp(-torch.sum(masked_log_probs, dim=1) / counts)
        if self.perplexities is None:
            self.perplexities = perplexities
        else:
            self.perplexities = torch.cat([self.perplexities, perplexities])

    def compute(self):
        if self.perplexities is None:
            raise NotComputableError('No samples have been given to compute perplexity.')
        return torch.mean(self.perplexities)

# Code for trainers borrowed from ignite/engine/__init__.py
def create_pred_trainer(model, criterion, optimizer):
    def _pred_update(engine, batch):
        model.train()
        optimizer.zero_grad()
        prediction = model(batch)
        loss = criterion(prediction, batch)
        loss.backward()
        optimizer.step()
        return prediction, batch, loss

    return Engine(_pred_update)

def create_pred_evaluator(model, metrics):
    def _inference(engine, batch):
        model.eval()
        with torch.no_grad():
            x = batch
            y_pred = model(x)
            return y_pred, x

    engine = Engine(_inference)

    for name, metric in metrics.items():
        metric.attach(engine, name)

    return engine

def save_model(model, filename):
    with open(filename, 'wb') as file_:
        torch.save(model, file_)

# tqdm handling taken from
# https://github.com/pytorch/ignite/blob/master/examples/mnist/mnist.py
def train_model(model, criterion, optimizer,
                train_loader, val_loader, log_interval=10, max_epochs=20,
                savefile=None):
    trainer = create_pred_trainer(model, criterion, optimizer)
    evaluator = create_pred_evaluator(model,
        metrics={'perplexity': Perplexity()})

    RunningAverage(output_transform=lambda output: output[-1]).attach(trainer,
                                                                      'average_loss')

    desc = "ITERATION {} - average loss: {:.2f}"
    pbar = tqdm(initial=0, leave=False, total=len(train_loader),
                desc=desc.format(0, 0))

    @trainer.on(Events.ITERATION_COMPLETED(every=log_interval))
    def log_training_loss(engine):
        pbar.desc = desc.format(engine.state.iteration,
                                engine.state.metrics['average_loss'])
        pbar.update(log_interval)

    @trainer.on(Events.EPOCH_COMPLETED)
    def log_training_results(engine):
        pbar.refresh()
        evaluator.run(train_loader)
        tqdm.write(
            "Training Results - Epoch: {}  Average perplexity {:.2f}  Average loss {:.2f}".format(
                engine.state.epoch, evaluator.state.metrics['perplexity'],
                engine.state.metrics['average_loss']
            )
        )

    @trainer.on(Events.EPOCH_COMPLETED)
    def log_validation_results(engine):
        evaluator.run(val_loader)
        tqdm.write(
            "Validation Results - Epoch: {} Average perplexity {:.2f}".format(
                engine.state.epoch, evaluator.state.metrics['perplexity']
            )
        )

        pbar.n = pbar.last_print_n = 0

    trainer.run(train_loader, max_epochs=max_epochs)
    pbar.close()
    if savefile:
        save_model(model, savefile)

def compute_perplexity(model, data_loader):
    evaluator = create_pred_evaluator(model,
        metrics={'perplexity': Perplexity()})
    evaluator.run(data_loader)
    return evaluator.state.metrics['perplexity']

def compute_perplexity_on_pan_dataset(model, preprocessor, dataset,
                                      device=None, batch_size=10):
    if not device:
        device = get_device()
    dataset = RawTextDataset(verification_dataset_text_iterator(dataset),
                             preprocessor, device=device)
    data_loader = DataLoader(dataset, batch_size)
    return compute_perplexity(model, data_loader)

def compute_perplexity_on_pan_dataset_from_path(model, preprocessor,
                                                dataset_path, **kwargs):
    dataset = load_verification_task_from_pan_format(dataset_path)
    return compute_perplexity_on_pan_dataset(
        model, preprocessor, dataset, **kwargs)

def save_model_and_preprocessor(model, preprocessor, filename):
    save_model(model, "{}_model.pkl".format(filename))
    with open(filename, 'wb') as file_:
        pickle.dump(preprocessor, "{}_preprocessor.pkl".format(filename))

def load_model_and_preprocessor(name, device=None):
    if not device:
        device = get_device()
    with open("{}_model.pkl".format(name), 'rb') as file_:
        model = torch.load(file_, map_location=device)
    with open("{}_preprocessor.pkl".format(name), 'rb') as file_:
        preprocessor = pickle.load(file_)
    return model, preprocessor

def get_device():
    return torch.device("cuda" if torch.cuda.is_available() else "cpu")
