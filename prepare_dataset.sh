#!/bin/bash
# Shell script that downloads and sets up the dataset
set -euo pipefail

mkdir -p data
wget "https://www.cs.biu.ac.il/~koppel/blogs/blogs.zip" -O data/blogs.zip
(cd data; unzip blogs.zip)

python convert_blogs_to_pan.py
