#!/bin/bash
# Create output directory for verifiers
mkdir -p out/verifiers

# Train GLAD model
(cd evaluation/glad; python glad-main.py --save_model ../../out/verifiers/glad_model --training ../../pan/verifier_train)

# Train BAFF models
python _train_baff_verifiers.py
