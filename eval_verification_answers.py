"""
Script to compare a PAN authorship verification task formatted answers file
with the groundtruth.
"""

import argparse

from sklearn.metrics import roc_auc_score
import numpy as np

from obfy.data import load_pred_file, load_truth_file

parser = argparse.ArgumentParser()
parser.add_argument("-t", "--truth",
                    help="Groundtruth to compare the predictions to")
parser.add_argument("-p", "--predict",
                    help="Predictions to compare against the groundtruth")
args = parser.parse_args()

groundtruth = load_truth_file(args.truth)
predictions = load_pred_file(args.predict)

def dtv(df):
    return np.array(df['label'], dtype=float)

def accuracy(a, b):
    a = dtv(a)
    b = dtv(b)
    return sum(a == b) / len(a)

acc = accuracy(predictions.round(), groundtruth)
roc = roc_auc_score(dtv(groundtruth), dtv(predictions))
print("# Scores of {}".format(args.predict))
print("(Against groundtruth {})".format(args.truth))
print("Accuracy: {:.3f}".format(acc))
print("AUROC: {:.3f}".format(roc))
