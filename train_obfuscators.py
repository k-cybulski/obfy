import pickle

from obfy.obfuscator import SynonymObfuscator, wnet_synonym, gensim_synonymizer
from obfy.data import (load_verification_task_from_pan_format,
                        verification_dataset_text_iterator,
                        save_verification_task_in_pan_format)

dataset = load_verification_task_from_pan_format('./pan/train')
dataset = dataset[:600] # obfuscators don't need that much data to train

obfuscator = SynonymObfuscator(wnet_synonym)
obfuscator.train(verification_dataset_text_iterator(dataset))

with open('out/obfuscator_wordnet.pkl', 'wb') as file_:
    pickle.dump(obfuscator, file_)

obfuscator.synonym_func = gensim_synonymizer()
with open('out/obfuscator_glove.pkl', 'wb') as file_:
    pickle.dump(obfuscator, file_)
