"""
Script that transforms the blog authorship corpus[1] into a form compatible with
PAN authorship verification shared tasks[2]

[1] http://u.cs.biu.ac.il/~koppel/BlogCorpus.htm
[2] https://pan.webis.de/clef15/pan15-web/authorship-verification.html
"""
import os

from obfy.data import (get_dataset, setup_verification_task,
                       save_verification_task_in_pan_format,
                       split_dataset)

DATA_DIR = "./data/blogs"
dataset = get_dataset(DATA_DIR)

# We will prepare PAN Shared Task compatible datasets so that we can compare
# the model to some previous works
train, val, test, verifier_train, trial, all_own_author = split_dataset(dataset, 
                                                 split_sizes=[0.3, 0.1, 0.05,
                                                              0.3, 0.05, 0.1])

PAN_DIR = "./pan"

for dataset, name in zip([train, val, test, verifier_train, trial,
                          all_own_author],
                      ['train', 'val', 'test', 'verifier_train', 'trial',
                       'all_own_author']):
    fraction_with_false_authors = (0.5 
                                   if name != 'all_own_author'
                                   else 0)
    verification_dataset = setup_verification_task(dataset,
                                                   fraction_with_false_authors=fraction_with_false_authors)
    save_verification_task_in_pan_format(verification_dataset, 
                                         os.path.join(PAN_DIR, name))
