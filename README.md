# Obfy
An experiment in authorship obfuscation. It is a complete project setup for
evaluating obfuscator performance.

# Getting started
## Setup
The setup script requires `wget` and `zip`
```
apt-get install wget zip
```

Furthermore, make sure to have the correct Python packages installed. Note that it is
useful to have a CUDA enabled machine for PyTorch code to run faster.
```
pip install -r requirements.txt
```

Having done the above, the dataset may be obtained and converted into a usable form
by running
```
bash prepare_dataset.sh
```
This will result in a `./data/blogs` directory with the original dataset as well as
`./pan` with PAN authorship verification shared task format data.

Also, it is necessary to have a few nltk specific models downloaded, however given
that models used vary heavily during development there is no comprehensive list.

## Running experiments
The project is made up of a few training scripts which produce pickle files that are
then used by a final obfuscation performance script which uses both authorship
verifiers and validity evaluation language models on data obfuscated by implemented
methods.

Training scripts:
```
bash train_verifiers.sh
python train_obfuscators.py
train_validity_evaluators.py
```

Final experiment runner, which builds up obfuscator+verifier+validator
cross-examination results:
```
python complete_experiment.py
```

# Acknowledgements
The [Groningen Lightweight Authorship
Detection](https://github.com/pan-webis-de/glad) code is included in with some updates
to make it run in `./evaluation/glad/`
