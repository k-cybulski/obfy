"""
This script loads pretrained obfuscators, verifiers and validity evaluators and
runs organised tests.
"""
import pickle
from pathlib import Path
import os
import subprocess
import shutil

from sklearn.metrics import roc_auc_score
import numpy as np

from obfy.data import (load_verification_task_from_pan_format,
                       save_verification_task_in_pan_format,
                        load_pred_file, load_truth_file,
                        verification_dataset_text_iterator)
from obfy.obfuscator import (SynonymObfuscator, wnet_synonym,
                             gensim_synonymizer, obfuscate_dataset,
                             RandomObfuscator)
from obfy.dl import (load_model_and_preprocessor,
                     compute_perplexity_on_pan_dataset)
from obfy.verifier import evaluate_on_dataset

def _load_pickle(filename):
    with open(filename, 'rb') as file_:
        return pickle.load(file_)

obfuscators = {
    'baseline': None,
    'synonymizer_wnet':  _load_pickle('./out/obfuscator_wordnet.pkl'),
    'synonymizer_glove': _load_pickle('./out/obfuscator_glove.pkl'),
    'random_obfuscator': RandomObfuscator()
}

verifiers = {
    'glad': None, # handled separately
    'baff_3char_tree': _load_pickle('./out/3char_tree.pkl'),
    'baff_tweet_lr': _load_pickle('./out/tweet_logistic_regression.pkl')
}

validators = {
    'lstm_char': load_model_and_preprocessor('./out/lstm_char'),
    'lstm_tweet': load_model_and_preprocessor('./out/lstm_tweet')
}

dataset = load_verification_task_from_pan_format('./pan/test')
dataset = dataset[:500]

# The random obfuscator is not prone to overfitting, as it simply inserts
# arbitrary words it has previously seen. Since it is here only as a silly
# baseline to evaluate performance of verifiers and validators under the
# assumption that it spews complete gibberish.
obfuscators['random_obfuscator'].train(verification_dataset_text_iterator(dataset))

def dtv(df):
    return np.array(df['label'], dtype=float)

def accuracy(a, b):
    a = dtv(a)
    b = dtv(b)
    return sum(a == b) / len(a)

def verifier_answers_metrics(truth_file, pred_file):
    groundtruth = load_truth_file(truth_file)
    predictions = load_pred_file(pred_file)
    acc = accuracy(predictions.round(), groundtruth)
    roc = roc_auc_score(dtv(groundtruth), dtv(predictions))
    return acc, roc

for obfuscator_name, obfuscator in obfuscators.items():
    print("Now examining {}".format(obfuscator_name))
    obf_exp_dir = os.path.join("out/obfuscation_experiments",
                      obfuscator_name)
    Path(obf_exp_dir).mkdir(exist_ok=True, parents=True)
    if not obfuscator:
        new_dataset = dataset
    else:
        new_dataset = obfuscate_dataset(obfuscator, dataset)
    save_verification_task_in_pan_format(new_dataset,
          os.path.join(obf_exp_dir, 'data'))
    for validator_name, validator in validators.items():
        print(" Now examining {}".format(validator_name))
        model, preprocessor = validator
        perplexity = compute_perplexity_on_pan_dataset(
            model, preprocessor, new_dataset)
        with open(os.path.join(obf_exp_dir,
                               '{}_perplexity.txt'.format(validator_name)), 'w') as file_:
            file_.write(str(perplexity))
    for verifier_name, verifier in verifiers.items():
        print(" Now examining {}".format(verifier_name))
        verifier_answers = os.path.join(obf_exp_dir,
                                         '{}_answers.txt'.format(verifier_name))
        if verifier_name != 'glad':
            evaluate_on_dataset(verifier, 
                            os.path.join(obf_exp_dir, 'data'),
                            verifier_answers)
        else:
            sb = subprocess.run('python evaluation/glad/glad-main.py -m out/glad_model -i {}'.format(obf_exp_dir).split(" "))
            shutil.move('answers.txt', verifier_answers)
        acc, roc = verifier_answers_metrics(
            os.path.join(obf_exp_dir, 'data', 'truth.txt'),
            verifier_answers)
        with open(os.path.join(obf_exp_dir,
                               '{}_scores.txt'.format(verifier_name)), 'w') as file_:
            file_.write("acc: {}\nroc: {}".format(acc, roc))
