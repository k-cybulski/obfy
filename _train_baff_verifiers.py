import pickle
import os

from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression

from obfy.data import load_verification_task_from_pan_format
from obfy.features import char_ngram_tokenizer, tweet_tokenizer
from obfy.verifier import BAFFVerifier, evaluate_on_dataset

TRAIN_SET = './pan/verifier_train'
TEST_SET = './pan/test'

# Which features are important for authorship verification?
# We can find this out by seeing how the model will perform
experiments = [
    ('tweet_logistic_regression', {'classifier': LogisticRegression(),
                                    'tokenizer': tweet_tokenizer()}),
    ('tweet_tree', {'classifier': DecisionTreeClassifier(),
                                    'tokenizer': tweet_tokenizer()}),
    ('3char_logistic_regression', {'classifier': LogisticRegression(),
                                    'tokenizer': char_ngram_tokenizer(3)}),
    ('3char_tree', {'classifier': DecisionTreeClassifier(),
                    'tokenizer': char_ngram_tokenizer(3)}),
    ('2char_tree', {'classifier': DecisionTreeClassifier(),
                    'tokenizer': char_ngram_tokenizer(2)}),
    ('1char_tree', {'classifier': DecisionTreeClassifier(),
                    'tokenizer': char_ngram_tokenizer(1)})
]

dataset = load_verification_task_from_pan_format('./pan/verifier_train')

for name, params in experiments:
    print("Experimenting with {}".format(name))
    verifier = BAFFVerifier(**params)
    verifier.train(dataset, verbose=True)
    evaluate_on_dataset(verifier, './pan/test',
                        os.path.join('./out/verification_feature_comparison',
                                     "{}.txt".format(name)))
    with open('./out/{}.pkl'.format(name), 'wb') as file_:
        pickle.dump(verifier, file_)
